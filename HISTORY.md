## vNEXT

## v2017.12.27

* `d3m.index` command tool rewritten to support three commands: `search`, `discover`,
  and `describe`. See details by running `python -m d3m.index -h`.
* Package now requires Python 3.6.
* Repository migrated to gitlab.com and made public.

## v2017.10.10

* Made `d3m.index` module with API to register primitives into a `d3m.primitives` module
  and searches over it.
* `d3m.index` is also a command-line tool to list available primitives and automatically
  generate JSON annotations for primitives.
* Created `d3m.primitives` module which automatically populates itself with primitives
  using Python entry points.
